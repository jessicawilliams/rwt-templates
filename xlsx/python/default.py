import xlsxwriter
import xlwt
import datetime
from rwt.helper_modules.document_maker.document_types.report import report


class default():
    """
    A class that deals with all the bits to create a default xlsx
    """

    def info(self):
        return {"document_type":"report", "icon":"fas fa-table"}

    def make(self, document_data, template_data, workbook):
        try:
            document_title = document_data["details"]["title"]
            if document_title.strip() == "":
                raise KeyError
        except:
            raise KeyError("You need to specify a document title..")

        # open the template_dataig
        titles = template_data["fields"]["findings"]
        ratings = template_data["ratings"]

        document_title = document_data["details"]["title"]

        # do initial summary sheet
        summary_sheet = workbook.add_worksheet('Document Summary')
        summary_sheet.hide_gridlines(2)

        todays_date = datetime.datetime.strftime(
            datetime.datetime.now(), "%d/%m/%Y")

        # add customer logo
        # if "client_logo" in document_data["details"]:
        # 	summary_sheet.insert_image(0,3, template_dataig.IMAGES_FOLDER + document_data["details"]["client_logo"].keys()[0], {'x_scale':0.5, 'y_scale':0.5})

        summary_sheet.set_footer(template_data["title"])

        summary_style = workbook.add_format(
            {"bold": True, "font_name": "Verdana", "valign": "vcenter", "bg_color": "#" + template_data["colors"]["h_color"]})
        sub_style = workbook.add_format(
            {"font_name": "Verdana", "valign": "vcenter", "bg_color": "#" + template_data["colors"]["subh_color"]})
        summary_style.set_border(1)
        sub_style.set_border(1)

        normal_style = workbook.add_format({"font_name": "Verdana"})

        summary_sheet.write(3, 0, 'Document Title', summary_style)
        summary_sheet.write(3, 1, template_data["title"].title(), normal_style)
        summary_sheet.write(4, 0, 'Date', summary_style)
        summary_sheet.write(4, 1, todays_date, normal_style)
        summary_sheet.write(5, 0, 'Document Version', summary_style)

        summary_style = workbook.add_format({"bold": True, "font_name": "Verdana", "valign": "vcenter",
                                             "bg_color": "#" + template_data["colors"]["h_color"], "align": "center"})
        sub_style = workbook.add_format({"font_name": "Verdana", "valign": "vcenter",
                                         "bg_color": "#" + template_data["colors"]["subh_color"], "align": "center"})
        summary_style.set_border(1)
        sub_style.set_border(1)

        summary_titles = template_data["fields"]["summary"]
        summary_sheet.write(8, 0, 'Date', summary_style)
        summary_sheet.set_column(0, 0, 30.5)
        summary_sheet.write(9, 0, todays_date, sub_style)
        summary_sheet.write(10, 0, '', sub_style)

        for c, t in enumerate(summary_titles):
            c = c + 1
            summary_sheet.set_column(c, c, 30.5)

            if isinstance(t, dict):
                summary_sheet.write(8, c, list(t.values())[0], summary_style)

                if list(t.keys())[0] in document_data["details"]:
                    summary_sheet.write(
                        9, c, document_data["details"][list(t.keys())[0]], sub_style)
                else:
                    summary_sheet.write(9, c, '', sub_style)
            else:
                summary_sheet.write(8, c, t)

                if t in document_data["details"]:
                    summary_sheet.write(
                        9, c, document_data["details"][t], sub_style)
                else:
                    summary_sheet.write(10, c, '', sub_style)

            summary_sheet.write(10, c, '', sub_style)

        summary_sheet.write(8, c + 1, 'Action', summary_style)
        summary_sheet.set_column(c + 1, c + 1, 30.5)
        summary_sheet.write(9, c + 1, 'DRAFT Agenda for Discussion', sub_style)
        summary_sheet.write(10, c + 1, 'Director QA and Release', sub_style)

        # do findings sheet
        sheet = workbook.add_worksheet(template_data["title"])
        sheet.hide_gridlines(2)

        # add customer logo
        # if "client_logo" in document_data["details"]:
        # 	sheet.insert_image(0,3, template_dataig.IMAGES_FOLDER + document_data["details"]["client_logo"].keys()[0], {'x_scale':0.5, 'y_scale':0.5 })

        header_style = workbook.add_format({"bold": True, "font_name": "Verdana", "valign": "vcenter",
                                            "align": "center", "bg_color": "#" + template_data["colors"]["h_color"]})
        header_style.set_border(1)
        header_style.set_border_color("white")

        sheet.set_row(3, 30)
        sheet.set_footer(template_data["title"])

        sheet.write(3, 0, "Ref.", header_style)

        for t, header in enumerate(titles):
            t = t + 1
            if isinstance(header, dict):
                sheet.write(3, t, list(list(header.values()))[0], header_style)
                sheet.set_column(t, t, (len(list(list(header.values()))[0])+4))
            else:
                sheet.write(3, t, header.replace(
                    "_", " ").title(), header_style)
                sheet.set_column(t, t, (len(header)+4))

        # count up all the findings
        document_data = report().f__bconvert_exploitability_and_risk(document_data, ratings)
        document_data = report().f__xput_into_list(document_data)
        # sort the findings by risk rating
        document_data = report().f__ysort_findings_by_risk(document_data, ratings)
        normal_style = workbook.add_format(
            {"font_name": "Verdana", "valign": "top", "text_wrap": True})
        normal_style.set_border(1)
        normal_style.set_border_color("white")

        row_counter = 4
        finding_counter = 1.01
        increment_step = .01
        for i, (id, c) in enumerate(document_data.get("report_findings", {}).get("components", {}).items()):
            # add little header for each component
            component_style = workbook.add_format(
                {"bold": True, "font_name": "Verdana", "valign": "vcenter", "bg_color": "#" + template_data["colors"]["component_color"]})
            component_style.set_border(1)
            component_style.set_border_color("white")
            sheet.merge_range(row_counter, 0, row_counter, len(
                titles), c['title'], component_style)

            row_counter = row_counter + 1

            if "findings" in c:
                for f, finding in enumerate(c["findings"]):
                    sheet.write(row_counter, 0, str(finding_counter))
                    for t, header in enumerate(titles):
                        t = t + 1
                        if header == "risk":
                            rating_style = workbook.add_format({"bold": True, "font_name": "Verdana", "align": "center", "valign": "vcenter",
                                                                "bg_color": "#" + str(finding["severity"]["color"]), "font_color": "#" + str(finding["severity"]["text_color"])})
                            rating_style.set_border(1)
                            rating_style.set_border_color("white")
                            sheet.write(row_counter, t,
                                        finding["severity"]["description"], rating_style)
                        else:
                            if isinstance(header, dict):
                                if list(list(header.keys()))[0] in finding:
                                    if isinstance(finding[list(list(header.keys()))[0]], list):
                                        host_string = ""
                                        for host in finding[list(list(header.keys()))[0]]:
                                            host_string = host_string + \
                                                host["host"] + ","
                                        sheet.write(row_counter, t,
                                                    host_string[:-1])
                                        # sheet.write(row_counter,t,', '.join(finding[list(list(header.keys()))[0]]))
                                    else:
                                        sheet.write(
                                            row_counter, t, finding[list(list(header.keys()))[0]])
                            else:
                                if header in finding:
                                    if isinstance(finding[header], list):
                                        sheet.write(row_counter, t,
                                                    ','.join(finding[header]))
                                    else:
                                        sheet.write(row_counter, t,
                                                    finding[header])

                    row_counter = row_counter + 1
                    finding_counter = round(finding_counter+increment_step, 2)

        workbook.close()

